Forms template tag
==================

The following template tags can be used to render form :ref:`fields<field>`, :ref:`field wrappers<fieldwrapper>`
and :ref:`field holders<fieldholder>`. To use these tags add the following at the top of your
template::

    {% load forms %}

.. _field:

``{% field %}``
---------------

The field tag can be used to render a form field. It can be used to
add or override any HTML attribute. Any keyword argument is added as HTML
attribute.

===================  ====================================================
Argument             Description
===================  ====================================================
``suppress_errors``  True to suppress adding an error class when value is
                     invalid.
===================  ====================================================

::

    {% field form.any_field %}
    <input ..>

    {% field form.any_field placeholder=_('Something') %}
    <input placeholder="Something" ..>

    {% field form.any_field class="special classes" %}
    <input class="special classes" ..>

.. note::
    To add HTML attributes with a hyphen for example ``data-example`` you
    should replace those hyphens with underscores ``data_example=``.

Class attribute
~~~~~~~~~~~~~~~

Any form field with a widget of ``TextInput`` or subclass of it will be
rendered with a ``class="input-field"`` attribute. In case you pass a class
attribute this class is still prepended.

::

    {% field form.input_field %}
    <input class="input-field" ..>

    {% field form.input_field class="special class" %}
    <input class="input-field special class" ..>

In case the form is validated and the field contains any errors the field will
be rendered with a ``class="error"`` attribute. The class will be appended to
any other classes that are defined.

::

    {% field form.input_field %}
    <input class="input-field error" ..>

    {% field form.input_field class="special class" %}
    <input class="input-field special class error" ..>

.. _fieldwrapper:

``{% fieldwrapper %}``
----------------------

Render a form field wrapped by a field wrapper. All unspecific fieldwrapper
arguments are passed down to the field_ tag. The field_ tag is used to render
the part of the field.

======================  ====================================================
Argument                Description
======================  ====================================================
``suppress_errors``     True to suppress adding an error class and error
                        list when value is invalid.
``fieldwrapper_class``  Add extra classes to the field wrapper.
``before_field``        Add any markup before the field.
``after_field``         Add any markup after the field.
``control_label``       Override the control label text in case of a
                        checkbox input type.
``unit_text_left``      This will prepend an unit icon to the field with
                        this value.
``unit_text_right``     This will append an unit icon to the field with
                        this value.
======================  ====================================================

::

    {% fieldwrapper form.any_field %}
    <div class="fieldwrapper fieldwrapper--{field_type} fieldwrapper--{widget_type} {fieldwrapper_class}">
        {before_field}
        <input ..>
        {after_field}
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
    </div>

**Variations**

A field wrapper varies in many ways. Those variations depend on the form
fields widget type. The following examples will display those variations.

.. note::
    In case of a **PasswordInput**, **DateTimeBaseInput** or
    **unit_text_right** the `after_field` argument gets overridden. In case of
    **unit_text_left** the `before_field` argument gets overridden.

``RadioSelect``
~~~~~~~~~~~~~~~

::

    <div class="fieldwrapper ..">
        {before_field}
        <ul id="id_any_field">
                ..
        </ul>
        {after_field}
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
    </div>

``Select``
~~~~~~~~~~

::

    <div class="fieldwrapper ..">
        {before_field}
        <div class="select">
            <select ..>
        </div>
        {after_field}
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
    </div>

``CheckboxInput``
~~~~~~~~~~~~~~~~~

::

    <div class="fieldwrapper ..">
        {before_field}
        <label class="control checkbox">
            <input ..>
            <span class="control-indicator"></span>
            <span class="control-label">{control_label | help_text | label}</span>
        </label>
        {after_field}
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
    </div>

.. note::
    When there is no specific **control_label** passed it will use the
    **help_text** or the field **label** as control label.
    In case it does have a help text, this text won't be added as normal help
    text anymore. If there is no help text on the field it will use it's label.

``PasswordInput``
~~~~~~~~~~~~~~~~~

::

    <div class="fieldwrapper .. fieldwrapper--icon ..">
        {before_field}
        <input ..>
        <button class="input-icon input-icon__password" type="button" tabindex="-1">
            <svg ..>
        </button>
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
    </div>

``DateTimeBaseInput``
~~~~~~~~~~~~~~~~~~~~~

::

    <div class="fieldwrapper .. fieldwrapper--icon ..">
        {before_field}
        <input ..>
        <label class="label--inline-edit" for="{id}" title="{label} wijzigen">
            <svg ..>
        </label>
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
    </div>

``unit_text_left``, ``unit_text_right``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    <div class="fieldwrapper .. fieldwrapper--unit ..">
        <i class="input-unit input-unit--left">{unit_text_left}</i>
        <input class="input-field input-field--unit-left" ..>
        {after_field}
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
    </div>

    <div class="fieldwrapper .. fieldwrapper--unit ..">
        {before_field}
        <input class="input-field input-field--unit-right" ..>
        <i class="input-unit input-unit--right">{unit_text_right}</i>
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
    </div>

    <div class="fieldwrapper .. fieldwrapper--unit ..">
        <i class="input-unit input-unit--left">{unit_text_left}</i>
        <input class="input-field input-field--unit-left input-field--unit-right" ..>
        <i class="input-unit input-unit--right">{unit_text_right}</i>
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
    </div>

.. note::
    `unit_text_left` and `unit_text_right` can only be used on fields with a
    **TextInput** widget or subclass of it and when there is no
    **fieldwrapper--icon** added.

.. _fieldholder:

``{% fieldholder %}``
---------------------

Render a field holder which contains a fieldwrapper_. All unspecific
field holder arguments are passed down to the fieldwrapper_ and field_ tag.

.. note::
    This tag also takes context_, see the context_ section for more
    information.

======================  ====================================================
Argument                Description
======================  ====================================================
``suppress_errors``     True to suppress adding an error class and error
                        list when value is invalid.
``fieldholder_class``   Add extra classes to the field holder.
``horizontal``          Add ``fieldholder--horizontal`` class to the field
                        holder.
``label_tag``           Override the label tag markup, can also be an empty
                        string to remove the tag.
``label``               Override the default label text.
======================  ====================================================

::

    {% fieldholder form.any_field %}
    <div class="fieldholder">
        <label for="any_field" title=" .. "> .. </label>
        <div class="fieldwrapper ..">
            ..
            <input ..>
            ..
        </div>
    </div>

    {% fieldholder form.any_field horizontal=True label_tag="<label>E.g.</label>" %}
    <div class="fieldholder fieldholder--horizontal">
        <label>E.g.</label>
        <div class="fieldwrapper ..">
            ..
            <input ..>
            ..
        </div>
    </div>

    {% fieldholder form.any_field label_tag="" %}
    <div class="fieldholder">
        <div class="fieldwrapper ..">
            ..
            <input ..>
            ..
        </div>
    </div>

    {% fieldholder form.any_field fieldholder_class="special classes" label="Example" %}
    <div class="fieldholder special classes">
        <label for"any_field" title="Example">Example</label>
        <div class="fieldwrapper ..">
            ..
            <input ..>
            ..
        </div>
    </div>

.. _fieldholder_inline:

``{% fieldholder_inline %}``
----------------------------

Render a field holder for inline (table) use. All unspecific
field holder arguments are passed down to the fieldwrapper_ and field_ tag.

.. note::
    Unit text arguments are ignored in case of inline field holders.

.. note::
    This tag also takes context_, see the context_ section for more
    information.

======================  ====================================================
Argument                Description
======================  ====================================================
``suppress_errors``     True to suppress adding an error class and error
                        list when value is invalid.
``fieldholder_class``   Add extra classes to the field holder.
``label_tag``           Override the label tag markup, can also be an empty
                        string to remove the tag (TextInput only).
======================  ====================================================

::

    {% fieldholder_inline form.any_field %}
    <td class="table__cell table__cell--inline-edit">
        <div class="fieldwrapper ..">
            ..
            <input ..>
            <label class="label--inline-edit" for="{id}" title="{label} wijzigen">
                <svg ..>
            </label>
        </div>
    </td>

    {% fieldholder_inline form.any_field label_tag="<label>E.g.</label>" %}
    <td class="table__cell table__cell--inline-edit">
        <div class="fieldwrapper ..">
            ..
            <input ..>
            <label>E.g.</label>
        </div>
    </td>

    {% fieldholder_inline form.any_field label_tag="" %}
    <td class="table__cell table__cell--inline-edit">
        <div class="fieldwrapper ..">
            ..
            <input ..>
        </div>
    </td>

    {% fieldholder_inline form.any_field fieldholder_class="special classes" %}
    <td class="table__cell table__cell--inline-edit special classes">
        <div class="fieldwrapper ..">
            ..
            <input ..>
            <label class="label--inline-edit" for="{id}" title="{label} wijzigen">
                <svg ..>
            </label>
        </div>
    </td>

**Variations**

An inline field holder varies in a few ways. Those variations depend on the form
fields widget type. The following examples will display those variations.

``Select``
~~~~~~~~~~

::

    <td class="table__cell table__cell--inline-edit ..">
        <div class="fieldwrapper select--inline-edit ..">
            ..
            <div class="select">
                <select ..>
            </div>
            ..
        </div>
    </td>

``TextInput``
~~~~~~~~~~~~~

::

    <td class="table__cell table__cell--inline-edit ..">
        <div class="fieldwrapper fieldwrapper--inline-edit ..">
            ..
            <input class="input-field input-field--inline-edit" ..>
            <label class="label--inline-edit" for="{id}" title="{label} wijzigen">
                <svg ..>
            </label>
        </div>
    </td>

.. _fieldwrapper_combined:

``{% fieldwrapper_combined %}``
-------------------------------

Render multiple form fields wrapped by a combined field wrapper. To pass
field specific arguments you need to prefix that argument with `field0_` where
zero is the index of the field. You can pass as many form fields as you like.

======================  ====================================================
Argument                Description
======================  ====================================================
``suppress_errors``     True to suppress adding an error class and error
                        list when value is invalid.
``fieldwrapper_class``  Add extra classes to the field wrapper.
======================  ====================================================

::

    {% fieldwrapper_combined form.field_1 form.field_2 %}
    <div class="fieldwrapper {fieldwrapper_class}">
        <div class="fieldwrapper-combined">
            <div class="fieldwrapper-combined__field fieldwrapper--{field_type} fieldwrapper--{widget_type}">
                {field0_before_field}
                <input ..>
                {field0_after_field}
            </div>
            <div class="fieldwrapper-combined__field fieldwrapper--{field_type} fieldwrapper--{widget_type}">
                {field1_before_field}
                <input ..>
                {field1_after_field}
            </div>
        </div>
        <div class="helptext">..</div>
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
        <ul class="errorlist">..</ul>
    </div>

    {% fieldwrapper_combined form.field_1 form.field_2 form.field_3 %}
    <div class="fieldwrapper {fieldwrapper_class}">
        <div class="fieldwrapper-combined">
            <div class="fieldwrapper-combined__field fieldwrapper--{field_type} fieldwrapper--{widget_type}">
                {field0_before_field}
                <input ..>
                {field0_after_field}
            </div>
            <div class="fieldwrapper-combined__field fieldwrapper--{field_type} fieldwrapper--{widget_type}">
                {field1_before_field}
                <input ..>
                {field1_after_field}
            </div>
            <div class="fieldwrapper-combined__field fieldwrapper--{field_type} fieldwrapper--{widget_type}">
                {field2_before_field}
                <input ..>
                {field2_after_field}
            </div>
        </div>
        <div class="helptext">..</div>
        <div class="helptext">..</div>
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
        <ul class="errorlist">..</ul>
        <ul class="errorlist">..</ul>
    </div>

**Combined field specific arguments**

==========================  ==================================================
Argument                    Description
==========================  ==================================================
``field#_before_field``     Add any markup before the field.
``field#_after_field``      Add any markup after the field.
``field#_unit_text_left``   This will prepend an unit icon to the field with
                            this value.
``field#_unit_text_right``  This will append an unit icon to the field with
                            this value.
==========================  ==================================================

**Variations**

A combined field wrapper varies in few ways, this is different from a
fieldwrapper_. Those variations depend on the form fields widget type.
The following examples will display those variations.

.. note::
    In case of a **PasswordInput**, **DateTimeBaseInput** or
    **unit_text_right** the `after_field` argument gets overridden. In case of
    **unit_text_left** the `before_field` argument gets overridden.

``TextInput``
~~~~~~~~~~~~~

::

    <div class="fieldwrapper ..">
        <div class="fieldwrapper-combined">
            <div class="fieldwrapper-combined__field ..">
                {field0_before_field}
                <input placeholder="{label | field0_placeholder}" ..>
                {field0_after_field}
            </div>
            <div class="fieldwrapper-combined__field ..">
                {field1_before_field}
                <input placeholder="{label | field1_placeholder}" ..>
                {field1_after_field}
            </div>
        </div>
        <div class="helptext">..</div>
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
        <ul class="errorlist">..</ul>
    </div>

.. note::
    In case of a **TextInput** or subclass of it (this also includes
    **PasswordInput** and **DateTimeBaseInput**) a default placeholder is
    added. This can be overridden by specifying the placeholder for those
    fields.

``Select``
~~~~~~~~~~

::

    <div class="fieldwrapper ..">
        <div class="fieldwrapper-combined">
            <div class="fieldwrapper-combined__field select ..">
                {field0_before_field}
                <select ..>
                {field0_after_field}
            </div>
            <div class="fieldwrapper-combined__field select ..">
                {field1_before_field}
                <select ..>
                {field1_after_field}
            </div>
        </div>
        <div class="helptext">..</div>
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
        <ul class="errorlist">..</ul>
    </div>

``PasswordInput``
~~~~~~~~~~~~~~~~~

::

    <div class="fieldwrapper ..">
        <div class="fieldwrapper-combined">
            <div class="fieldwrapper-combined__field fieldwrapper--icon ..">
                {field0_before_field}
                <input placeholder="{label | field0_placeholder}" ..>
                <button class="input-icon input-icon__password" type="button" tabindex="-1">
                    <svg ..>
                </button>
            </div>
            <div class="fieldwrapper-combined__field fieldwrapper--icon ..">
                {field1_before_field}
                <input placeholder="{label | field1_placeholder}" ..>
                <button class="input-icon input-icon__password" type="button" tabindex="-1">
                    <svg ..>
                </button>
            </div>
        </div>
        <div class="helptext">..</div>
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
        <ul class="errorlist">..</ul>
    </div>

``DateTimeBaseInput``
~~~~~~~~~~~~~~~~~~~~~

::

    <div class="fieldwrapper ..">
        <div class="fieldwrapper-combined">
            <div class="fieldwrapper-combined__field fieldwrapper--icon ..">
                {field0_before_field}
                <input placeholder="{label | field0_placeholder}" ..>
                <label class="label--inline-edit" for="{id}" title="{label} wijzigen">
                    <svg ..>
                </label>
            </div>
            <div class="fieldwrapper-combined__field fieldwrapper--icon ..">
                {field1_before_field}
                <input placeholder="{label | field1_placeholder}" ..>
                <label class="label--inline-edit" for="{id}" title="{label} wijzigen">
                    <svg ..>
                </label>
            </div>
        </div>
        <div class="helptext">..</div>
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
        <ul class="errorlist">..</ul>
    </div>

``unit_text_left``, ``unit_text_right``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    <div class="fieldwrapper ..">
        <div class="fieldwrapper-combined">
            <div class="fieldwrapper-combined__field fieldwrapper--unit ..">
                <i class="input-unit input-unit--left">{field0_unit_text_left}</i>
                <input class="input-field input-field--unit-left" placeholder="{label | field0_placeholder}" ..>
                {field0_after_field}
            </div>
            <div class="fieldwrapper-combined__field fieldwrapper--unit ..">
                {field1_before_field}
                <input class="input-field input-field--unit-right" placeholder="{label | field1_placeholder}" ..>
                <i class="input-unit input-unit--right">{field1_unit_text_right}</i>
            </div>
            <div class="fieldwrapper-combined__field fieldwrapper--unit ..">
                <i class="input-unit input-unit--left">{field2_unit_text_left}</i>
                <input class="input-field input-field--unit-left input-field--unit-right" placeholder="{label | field2_placeholder}" ..>
                <i class="input-unit input-unit--right">{field2_unit_text_right}</i>
            </div>
        </div>
        <div class="helptext">..</div>
        <div class="helptext">..</div>
        <div class="helptext">..</div>
        <ul class="errorlist">..</ul>
        <ul class="errorlist">..</ul>
        <ul class="errorlist">..</ul>
    </div>

.. note::
    `unit_text_left` and `unit_text_right` can only be used on fields with a
    **TextInput** widget or subclass of it and when there is no
    **fieldwrapper--icon** added.

.. _fieldholder_combined:

``{% fieldholder_combined %}``
------------------------------

Render a field holder which contains a fieldwrapper_combined_. All unspecific
field holder arguments are passed down to the fieldwrapper_combined_ tag.
This tag also allows passing multiple form fields, as much as you like.

.. note::
    This tag also takes context_, see the context_ section for more
    information.

======================  ====================================================
Argument                Description
======================  ====================================================
``suppress_errors``     True to suppress adding an error class and error
                        list when value is invalid.
``fieldholder_class``   Add extra classes to the field holder.
``horizontal``          Add ``fieldholder--horizontal`` class to the field
                        holder.
``label_tag``           Override the label tag markup, can also be an empty
                        string to remove the tag.
``label``               Override the default label text.
======================  ====================================================

::

    {% fieldholder_combined form.field_1 form.field_2 %}
    <div class="fieldholder">
        <label for="field_1" title=" .. "> .. </label>
        <div class="fieldwrapper ..">
            <div class="fieldwrapper-combined">
                <div class="fieldwrapper-combined__field ..">
                    ..
                    <input ..>
                    ..
                </div>
                <div class="fieldwrapper-combined__field ..">
                    ..
                    <input ..>
                    ..
                </div>
            </div>
            ..
        </div>
    </div>

    {% fieldholder_combined form.field_1 form.field_2 horizontal=True label_tag="<label>E.g.</label>" %}
    <div class="fieldholder fieldholder--horizontal">
        <label>E.g.</label>
        <div class="fieldwrapper ..">
            <div class="fieldwrapper-combined">
                <div class="fieldwrapper-combined__field ..">
                    ..
                    <input ..>
                    ..
                </div>
                <div class="fieldwrapper-combined__field ..">
                    ..
                    <input ..>
                    ..
                </div>
            </div>
            ..
        </div>
    </div>

    {% fieldholder_combined form.field_1 form.field_2 label_tag="" %}
    <div class="fieldholder">
        <div class="fieldwrapper ..">
            <div class="fieldwrapper-combined">
                <div class="fieldwrapper-combined__field ..">
                    ..
                    <input ..>
                    ..
                </div>
                <div class="fieldwrapper-combined__field ..">
                    ..
                    <input ..>
                    ..
                </div>
            </div>
            ..
        </div>
    </div>

    {% fieldholder_combined form.field_1 form.field_2 fieldholder_class="special classes" label="Example" %}
    <div class="fieldholder special classes">
        <label for"field_1" title="Example">Example</label>
        <div class="fieldwrapper ..">
            <div class="fieldwrapper-combined">
                <div class="fieldwrapper-combined__field ..">
                    ..
                    <input ..>
                    ..
                </div>
                <div class="fieldwrapper-combined__field ..">
                    ..
                    <input ..>
                    ..
                </div>
            </div>
            ..
        </div>
    </div>

.. _hidden_fields:

``{% hidden_fields %}``
-----------------------

Render all hidden fields from a form. Usage example::

    {% hidden_fields form %}
    <input type="hidden" .. >
    <input type="hidden" .. >
    ..

.. _non_field_errors:

``{% non_field_errors %}``
--------------------------

Render a block element that holds a list of all no field errors.
Usage example::

    {% non_field_errors form %}
    <div class="inline-validation inline-validation--error">
        <ul class="errorlist nonfield">
            ..
        </ul>
    </div>

.. _non_form_errors:

``{% non_form_errors %}``
-------------------------

Render a block element that holds a list of all no form errors (expects a
formset as first argument).
Usage example::

    {% non_form_errors formset %}
    <div class="inline-validation inline-validation--error">
        <ul class="errorlist">
            ..
        </ul>
    </div>

.. _context:

Context
-------

In case a template tag takes context it means any variable that is available
in the template also is available for the template tag.
This way a few arguments can also be passed / set by context. This is the
case for **suppress_errors** and **horizontal**. Common use cases are::

    {# This will render all field holders with the fieldholder--horizontal class #}
    {% with horizontal=True %}
        ..
        {% fieldholder form.field_1 %}
        {% fieldholder form.field_2 %}
        {% fieldholder form.field_3 %}
        ..
        {% fieldholder form.field_4 %}
        ..
    {% endwith %}

    {# In case of inline formsets it's common to suppress errors when the form is marked for deletion #}
    <tr>
        {% with suppress_errors=form.DELETE.value %}
            {% fieldholder_inline form.field_1 %}
            {% fieldholder_inline form.field_2 %}
            {% fieldholder_inline form.field_3 %}
        {% endwith %}
        ..
    </tr>

