============
Installation
============

At the command line::

    $ easy_install django-form-tags

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-form-tags
    $ pip install django-form-tags
